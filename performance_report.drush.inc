<?php

/**
 * @file
 * This file implements support for drush into the module.
 */

/**
 * Implements hook_drush_help().
 */
function performance_report_drush_help($command) {
  switch ($command) {
    case 'drush:apc-clear':
      return dt('Clears the APC cache (you should not that this is for all sites on this machine).');
  }
}

/**
 * Implements hook_drush_command().
 */
function performance_report_drush_command() {
  $items = array();

  $items['apc-clear'] = array(
    'description' => dt('Clear APC.'),
    'aliases' => array('apc'),
  );

  return $items;
}

/**
 * Callback function for drush apc-clear command.
 */
function drush_performance_report_apc_clear() {
  //log to the command line with an OK status
  if (function_exists('apc_clear_cache')) {

    if (drush_get_context('DRUSH_DRUPAL_ROOT')) {
      if (drush_get_context('DRUSH_DRUPAL_SITE_ROOT')) {
        $uri = drush_get_context('DRUSH_URI');
        if ($uri == 'http://default') {
          drush_set_error('Unable to find site URI. Plase use -l or --uri option.');
          return;
        }
      }
    }

    drush_log('You are about to clear the systems Alternative PHP Cache', 'ok');
    if (drush_confirm('Are you sure you want to continue?', $indent = 0)) {
      // It's not possible to clear the web-servers cache from CLI, as it only
      // clears the CLI cache. So to over come this the module have a URL that
      // can be called with a token. We do not follow redirects as that would
      // result in a redirect to a page with access denied (the URL is used
      // inside the module to, hence the redirect is allow there).
      $url = $uri . '/performance_report/apc/clear/' . drupal_get_hash_salt();
      $respone = drupal_http_request($url, array('max_redirects' => 0));
      if (isset($respone->error)) {
        drush_set_error('HTTP request faild: ' . $respone->code . ' - ' . $respone->error);
        drush_set_error('APC was NOT cleared');
      }
      else {
        drush_log('APC cleared', 'ok');
      }
    }
  }
  else {
    drush_set_error('The APC could not be cleared as APC is not available.');
  }
}

