<?php
/**
 * @file
 * Adds limited support for varnish in the form of a cache clear form. Varnish
 * 3.x do not have support for getting performance information directly.
 */

$plugin = array(
  'requirements' => array(
    'function' => 'performance_report_varnish_requirements',
  ),
  'data' => array(
    'function' => 'performance_report_varnish_data',
  ),
);

/**
 * Checkes that the varnish module is available as it is required to communicate
 * with varnish.
 *
 * @return array $requirements
 *  An array with requirement messages if any not met.
 */
function performance_report_varnish_requirements() {
  $requirements = array();

  // Check that the varnish module is actived.
  if (!function_exists('varnish_get_status')) {
    $requirements['installed'] = array(
      'title' => t('Varnish not installed'),
      'message' => t('Varnish module was not detected. If you are using varnish you should install the module.'),
      'value' => FALSE,
    );
  }

  return $requirements;
}

/**
 * Returns report information, which in this cache is a simple form the clear
 * the cache.
 *
 * @return array
 *  Returns a render array with a message and the purge form.
 */
function performance_report_varnish_data() {
  return array(
    '#theme' => 'performance_report',
    '#title' => t('Varnish'),
    '#description' => t('Remote HTTP proxy used to cache pages.'),

    // Get purge uri parts form.
    '#actions' => drupal_get_form('performance_report_varnish_form'),
  );
}

/**
 * Implements the form that provides the varnish cache clear actions.
 *
 * @return array $form
 *  The array representing the form.
 */
function performance_report_varnish_form() {
  $form = array();

  $form['description'] = array(
    '#markup' => t('<em>Varnish 3.x have a very limit support for getting performance information. Therefore this extension only allows you to clean the cache or paths in the cache.</em>'),
  );

  $form['pattern'] = array(
    '#type' => 'textfield',
    '#title' => t('URI (site paths)'),
    '#description' => t('Enter relative URL to remove/invalidate from Varnish. You can enter comman sperated values to clear more than one path.'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Clear path(s)'),
  );

  $form['clear_all'] = array(
    '#type' => 'button',
    '#value' => t('Clear all paths'),
  );

  return $form;
}

/**
 * Form validation function for the varnish cache clear form.
 *
 */
function performance_report_varnish_form_validate($form, &$form_state) {
  if ($form_state['clicked_button']['#id'] == 'edit-clear-all') {
    // Clear all paths have been clicked.
    if (!performance_report_varnish_purge(TRUE)) {
      drupal_set_message(t('Clear cache for this site have been sent to Varnish.'));
    }
  }
  else {
    // Check that a patteren/path(s) have been implemented.
    if (empty($form_state['values']['pattern'])) {
      form_set_error('pattern', 'You have not enter a Drupal path to clear Varnish cache for.');
      return;
    }
    else {
      // Validate that the path entered is a valided paht in drupal.
      $paths = explode(',', $form_state['values']['pattern']);
      foreach ($paths as $path) {
        if (!drupal_valid_path(trim($path))) {
          form_set_error('pattern', check_plain($path) . ' is not a valid Drupal path.');
          return;
        }
      }
    }
  }
}

/**
 * Submittion function for the varnish cache clear form.
 *
 */
function performance_report_varnish_form_submit($form, &$form_state) {
  // Get paths and clear them one by one.
  $paths = explode(',', $form_state['values']['pattern']);
  if (!performance_report_varnish_purge(FALSE, $paths)) {
    drupal_set_message(t('Clear cache for the path(s) have been sent to Varnish.'));
  }
}

/**
 * Helper function that clears the remote varnish cache. Either all paths or the
 * paths enterd in the form.
 *
 * @param type $all
 *  If TRUE all paths are cleared, defaults to FALSE.
 * @param type $paths
 *  Array of path(s) to clear form the cache.
 * @return boolean
 *  TRUE on succes else FALSE.
 */
function performance_report_varnish_purge($all = FALSE, $paths = array('.')) {
  if ($all) {
    $paths = array(base_path());
  }

  // Find host and command to use to purge varnish.
  $host = _varnish_get_host();
  $version = floatval(variable_get('varnish_version', 2.1));
  $command = $version >= 3 ? "ban" : "purge";
  foreach ($paths as $path) {
    $ret = _varnish_terminal_run(array($command . ' req.http.host ~ ' . $host . ' && req.url ~ "' . trim($path) . '"'));

    // Handle possible errors.
    $error = FALSE;
    if ($ret === FALSE) {
      drupal_set_message(t('<strong>Varnish</strong>: PHP socket extension not found'), 'error');
    }
    else {
      foreach ($ret as $terminal => $value) {
        if ($value === FALSE) {
          // Something went wrong, so we set an error message for that terminal.
          drupal_set_message(t('<strong>Varnish</strong>: Unable to clear path at %term', array('%term' => $terminal)), 'error');
          $error = TRUE;
        }
      }
    }
    return $error;
  }
}
