<?php
/**
 * @file
 * Get information about the running memcache instances on the system and checks
 * that memcache is running.
 */

$plugin = array(
  'requirements' => array(
    'function' => 'performance_report_memcache_requirements',
  ),
  'data' => array(
    'function' => 'performance_report_memcache_data',
  ),
  'alerts' => array(
    'function' => 'performance_report_memcache_alerts',
  ),
);

/**
 * Check that one of the PHP memcache extension is available and that the
 * memcache module is used as backend.
 *
 * @return array $requirements
 *  Returns an array with messages if any requirements are not met.
 */
function performance_report_memcache_requirements() {
  $requirements = array();

  // Check for memcache extension.
  $extension = FALSE;
  if (class_exists('Memcache')) {
    $extension = 'Memcache';
  }
  elseif (class_exists('Memcached')) {
    $extension = 'Memcached';
  }

  if (!$extension) {
    $requirements['installed'] = array(
      'title' => t('Memcache extension not instaled'),
      'message' => t('PHP Memcache extension was not found, you should install the extension.'),
      'value' => FALSE,
    );
  }

  if (!class_exists('MemCacheDrupal')) {
    $requirements['module'] = array(
      'title' => t('Memcache module not found'),
      'message' => t('Drupal memcache cache backend not found. Install the Memcache module correctly to resolve this issue.'),
      'value' => FALSE,
    );
  }

  return $requirements;
}

/**
 * Connect to the different memcache backends and get usage information from
 * them.
 *
 * @return array $data
 *  Render array with information about memory usage.
 */
function performance_report_memcache_data() {
  $data = array();

  // Get configured bins.
  $bins = variable_get('memcache_bins', array('cache' => 'default'));

  // Get stats for each cache bin and merge the information into each server.
  // If two bins are on the same server, the stats for the bins are the same,
  // so only server leve stats are collected.
  foreach ($bins as $bin => $name) {
    $stats = dmemcache_stats($bin);
    if (isset($stats[$bin])) {
      foreach ($stats[$bin] as $server => $values) {
        if (!isset($data[$server])) {
          $name = explode(':', $server);
          $data[$server] = array(
            '#theme' => 'performance_report',
            '#title' => $name[0] . ' (' . $name[1] . ')',
            '#collapsed' => FALSE,
          );
        }

        // Create index in data array.
        if (isset($data[$server]['bins'])) {
          $data[$server]['#data']['bins']['value'] = $bin;
        }
        else {
          $data[$server]['#data']['bins'] = array(
            'title' => t('Bin(s)'),
            'value' => $bin,
          );
        }

        // Check that the memcache server is running.
        if ($values['pid'] == -1) {
          drupal_set_message(t('The server seems down. No connection or response was received.'), 'error');
          continue;
        }

        // Memory usage.
        $data[$server]['#data']['memory_available'] = array(
          'title' => t('Available'),
          'value' => performance_report_bsize((real)$values['limit_maxbytes']),
        );
        $data[$server]['#data']['memory_free'] = array(
          'title' => t('Free'),
          'value' => performance_report_bsize($values['limit_maxbytes'] - $values['bytes']),
        );
        $data[$server]['#data']['memory_used'] = array(
          'title' => t('Used'),
          'value' => performance_report_bsize((real)$values['bytes']),
        );

        // Bytes out/in.
        $data[$server]['#data']['bytes_read'] = array(
          'title' => t('Data read'),
          'value' => performance_report_bsize((real)$values['bytes_read']),
        );
        $data[$server]['#data']['bytes_written'] = array(
          'title' => t('Data written'),
          'value' => performance_report_bsize((real)$values['bytes_written']),
        );

        // Calculate hit/miss percentages.
        $hits = sprintf("%.1f%%", 0);
        if ($values["cmd_get"] > 0) {
          $hits = sprintf("%.1f%%", ((real)$values["get_hits"] / (real)$values["cmd_get"] * 100));
        }
        $data[$server]['#data']['hits'] = array(
          'title' => t('Hits'),
          'value' => $values["get_hits"] . ' (' . $hits . ')',
        );

        $misses = sprintf("%.1f%%", 0);
        if ($values["cmd_get"] > 0) {
          $misses = sprintf("%.1f%%", 100 - ((real)$values["get_hits"] / (real)$values["cmd_get"] * 100));
        }
        $data[$server]['#data']['misses'] = array(
          'title' => t('Misses'),
          'value' => $values["cmd_get"] - $values["get_hits"] . ' (' . $misses . ')',
        );

        $data[$server]['#data']['version'] = array(
          'title' => t('Version'),
          'value' => $values['version'],
        );
        $data[$server]['#data']['uptime'] = array(
          'title' => t('Uptime'),
          'value' => format_interval($values['uptime'], 3),
        );
        $data[$server]['#data']['pid'] = array(
          'title' => t('Process ID'),
          'value' => $values['pid'],
        );
        $data[$server]['#data']['threads'] = array(
          'title' => t('Threads'),
          'value' => $values['threads'],
        );
        $data[$server]['#data']['curr_items'] = array(
          'title' => t('Current items'),
          'value' => $values['curr_items'],
        );
        $data[$server]['#data']['total_items'] = array(
          'title' => t('Total items'),
          'value' => $values['total_items'],
        );
        $data[$server]['#data']['evictions'] = array(
          'title' => t('Evictions'),
          'value' => $values['evictions'],
        );
      }
    }
    else {
      drupal_set_message(t('Unable to connect to memcache bin "%bin" to get information', array('%bin' => $bin)), 'warning');
    }
  }

  // Build the render array.
  $output = array(
    '#theme' => 'performance_report',
    '#title' => t('Memcache'),
    '#description' => t('Moves cache tables into memory.'),
    '#children' => $data,
  );

  return $output;
}

/**
 * Get information about memcache instances to check status and based on that
 * creates alerts.
 *
 * @return array $alerts
 *  An array with alert messages if any else an empty array.
 */
function performance_report_memcache_alerts() {
  $alerts = array();

  // Get configured bins.
  $bins = variable_get('memcache_bins', array('cache' => 'default'));

  // Get stats for each cache bin and merge the information into each server.
  // If two bins are on the same server, the stats for the bins are the same,
  // so only server leve stats are collected.
  foreach ($bins as $bin => $name) {
    $stats = dmemcache_stats($bin);
    if (isset($stats[$bin])) {
      foreach ($stats[$bin] as $server => $values) {
        if (!isset($data[$server])) {
          $name = explode(':', $server);
          $name = $name[0] . ' (' . $name[1] . ')';
        }

        $free = $values['limit_maxbytes'] - $values['bytes'];
        if ($free < 512 * 1024) {
          // Set alert, if less then 512 Kb of memory is free.
          $alerts[] = array(
            'title' => t('Out of memory'),
            'message' => t('<strong>Memcache</strong>: %name needs to have more memory allocated (free: %free).', array('%name' => $name, '%free' => performance_report_bsize($free))),
          );
        }

        // Cache hits.
        if (!$values["get_hits"]) {
          $alerts[] = array(
            'title' => t('No cache hits'),
            'message' => t('<strong>Memcache</strong>: %name have no cache hits.', array('%name' => $name)),
          );
        }

        // Cache evictions.
        if ($values['evictions']) {
          $alerts[] = array(
            'title' => t('Evictions found'),
            'message' => t('<strong>Memcache</strong>: %name have evictions, which may mean that it have to little memory allocated.', array('%name' => $name)),
          );
        }
      }
    }
    else {
      drupal_set_message(t('Unable to connect to memcache bin "%bin" to get information', array('%bin' => $bin)), 'warning');
    }

  }

  return $alerts;
}