<?php
/**
 * @file
 * Get information about APC cache usage and add support for clearing APC from
 * inside drupal.
 */

$plugin = array(
  'requirements' => array(
    'function' => 'performance_report_apc_requirements',
  ),
  'data' => array(
    'function' => 'performance_report_apc_data',
  ),
  'alerts' => array(
    'function' => 'performance_report_apc_alerts',
  ),
  'menu' => array(
    'function' => 'performance_report_apc_menu',
  ),
);

/**
 * Checks that APC extension is installed.
 *
 * @return array $requirements
 *  An array with messages or empty if all requirements are met.
 */
function performance_report_apc_requirements() {
  $requirements = array();

  // Check php version.
  if (!function_exists('apc_cache_info')) {
    $requirements['installed'] = array(
      'title' => t('APC not installed'),
      'message' => t('APC cache could not be detected, you should install APC.'),
      'value' => FALSE,
    );
  }

  return $requirements;
}

/**
 * Get APC usage information and add the clear APC button.
 *
 * @return array $data
 *  A render array with the information collected.
 */
function performance_report_apc_data() {
  // Get information about the current cache.
  $time = time();
  $memory = apc_sma_info();
  $cache = apc_cache_info();

  // Extract the information that we wants to look at.
  $data = array(
    'memory' => array(
      'title' => t('Available'),
      'value' => performance_report_bsize($memory['seg_size']),
    ),
    'memory_available' => array(
      'title' => t('Free'),
      'value' => performance_report_bsize($memory['avail_mem']),
    ),
    'file_memory' => array(
      'title' => t('Used'),
      'value' => performance_report_bsize($cache['mem_size'])
    ),
    'hits' => array(
      'title' => t('Hits'),
      'value' => $cache['num_hits'] . ' (' . sprintf("%.1f%%", $cache['num_hits'] * 100 / ($cache['num_hits'] + $cache['num_misses'])) . ')',
    ),
    'misses' => array(
      'title' => t('Misses'),
      'value' => $cache['num_misses'] . ' (' . sprintf("%.1f%%", $cache['num_misses'] * 100 / ($cache['num_hits'] + $cache['num_misses'])) . ')',
    ),
    'files' => array(
      'title' => t('Entries'),
      'value' => $cache['num_entries'],
    ),
  );

  // Prevent division by zero after cache clear
  if ($cache['start_time'] == $time) {
    $cache['start_time']--;
  }

  $data += array(
    'req_rate' => array(
      'title' => t('Request rate'),
      'value' => sprintf("%.2f", ($cache['num_hits'] + $cache['num_misses']) / ($time - $cache['start_time'])),
    ),
    'hit_rate' => array(
      'title' => t('Hit rate'),
      'value' => sprintf("%.2f", ($cache['num_hits']) / ($time - $cache['start_time'])),
    ),
    'miss_rate' => array(
      'title' => t('Miss rate'),
      'value' => sprintf("%.2f", ($cache['num_misses']) / ($time - $cache['start_time'])),
    ),
    'insert_rate' => array(
      'title' => t('Insert rate'),
      'value' => sprintf("%.2f", ($cache['num_inserts']) / ($time - $cache['start_time'])),
    ),
    'uptime' => array(
      'title' => t('Last cleared'),
      'value' => format_interval($time - $cache['start_time'], 3),
    ),
    'cache_punges' => array(
      'title' => t('Expunges'),
      'value' => $cache['expunges'],
    ),
  );

  return array(
    '#theme' => 'performance_report',
    '#title' => t('Alternative PHP Cache'),
    '#description' => t('Up-code cache that store byte compiled PHP code.'),
    '#data' => $data,

    // Add clear APC button.
    '#actions' => array(
      '#markup' => l(t('Clear APC'), 'performance_report/apc/clear/' . drupal_get_hash_salt(), array(
        'attributes' => array(
          'class' => array(
            'button',
            'apc-clear-button',
          ),
        ),
      )),
    ),
  );
}

function performance_report_apc_alerts() {
  $alerts = array();

  $memory = apc_sma_info();
  if ($memory['avail_mem'] < 100) {
    // Set alert if less then 100 bytes are free.
    $alerts[] = array(
      'title' => t('Out of memory'),
      'message' => t('APC needs more memory (free: @free Bytes)', array('@free' => $memory['avail_mem'])),
    );
  }

  $cache = apc_cache_info();
  if (!$cache['num_hits']) {
    $alerts[] = array(
      'title' => t('No cache hit'),
      'message' => t('<strong>APC</strong>: the cache have not hits, so it may not work.'),
    );
  }

  return $alerts;
}

/**
 * Add menu callback to clear out APC.
 *
 * @return array $items
 *  An array with menu items.
 */
function performance_report_apc_menu() {
  $items = array();

  $items['performance_report/apc/clear/%'] = array(
    'title' => 'Clear APC',
    'description' => 'Clears the APC',
    'page callback' => 'performance_report_apc_clear',
    'page arguments' => array(3),
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
    'file path' => drupal_get_path('module', 'performance_report') . '/plugins/performance_report/',
    'file' => 'apc.inc',
  );

  return $items;
}

/**
 * Clear APC.
 *
 * @param string $token
 *  Drupal hash salt to ensure that the function is called securely.
 */
function performance_report_apc_clear($token) {
  if (function_exists('apc_clear_cache') && $token == drupal_get_hash_salt()) {
    apc_clear_cache();
    drupal_set_message(t('The APC have been cleared'));
  }
  else {
    drupal_set_message(t('The APC could not be cleared as APC is not available'), 'warning');
  }

  drupal_goto('admin/reports/performance_report');
}