<?php
/**
 * @file
 * Adds support for checking PHP 5.3+ real path cache usage.
 */

$plugin = array(
  'requirements' => array(
    'function' => 'performance_report_realpath_requirements',
  ),
  'data' => array(
    'function' => 'performance_report_realpath_data',
  ),
  'alerts' => array(
    'function' => 'performance_report_realpath_alerts',
  ),
);

/**
 * Checks that PHP version is grater than 5.3 as real path cache first exists in
 * this version.
 *
 * @return array $requirements
 *  An array with requirement messages if any not met.
 */
function performance_report_realpath_requirements() {
  $requirements = array();

  // Check php version is grater than 5.3.
  if (floatval(phpversion()) < 5.3) {
    $requirements['version'] = array(
      'title' => t('PHP version'),
      'message' => t('You should really upgrade your version of PHP to 5.3 or newer.'),
      'value' => phpversion(),
    );
  }
  return $requirements;
}

/**
 * Get memory information about real path cache usage.
 *
 * @return array
 *  Render array with the information collected.
 */
function performance_report_realpath_data() {
  // Get information about the real path cache usage.
  $realpath_cache_size = (int)str_ireplace( 'k', '', ini_get('realpath_cache_size'));
  $realpath_cache_used = number_format((realpath_cache_size() / 1024), 2);
  $realpath_cache_remaining = ($realpath_cache_size - $realpath_cache_used);
  $realpath_cache_entries = count(realpath_cache_get());

  // Build return data array.
  $data = array(
    'realpath_size' => array(
      'title' => t('Size'),
      'value' => performance_report_bsize($realpath_cache_size * 1024),
    ),
    'realpath_used' => array(
      'title' => t('Used'),
      'value' => performance_report_bsize($realpath_cache_used * 1024),
    ),
    'realpath_remaining' => array(
      'title' => t('Free'),
      'value' => performance_report_bsize($realpath_cache_remaining * 1024),
    ),
    'realpath_entries' => array(
      'title' => t('Entries'),
      'value' => $realpath_cache_entries . ' files',
    ),
  );

  return array(
    '#theme' => 'performance_report',
    '#title' => t('Real path cache'),
    '#description' => t('Cache of include paths, which speeds up site performance.'),
    '#data' => $data,
    '#actions' => drupal_get_form('performance_report_realpath_form'),
  );
}

/**
 * Implements the action button to toggle injection of real path stats in to the
 * page footer.
 *
 * @return array $form
 */
function performance_report_realpath_form() {
  $form = array();

  $form['description'] = array(
    '#markup' => '<p>' . t('Display real path information in the footer on every page load.') . '</p>',
  );

  $log = variable_get('performance_report_realpath_stats', FALSE);
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => $log ? t('Disable stats') : t('Enable stats'),
  );

  return $form;
}

/**
 * Submit function that toggles the state of real path footer injection.
 */
function performance_report_realpath_form_submit($form, &$form_state) {
  variable_set('performance_report_realpath_stats', !variable_get('performance_report_realpath_stats', FALSE));
}

/**
 * Set an alert if we are runing low on free real path cache memory.
 *
 * @return array $alerts
 *  An array with alert messages.
 */
function performance_report_realpath_alerts() {
  $alerts = array();

  $realpath_cache_size = (int)str_ireplace( 'k', '', ini_get('realpath_cache_size'));
  $realpath_cache_used = realpath_cache_size() / 1024;
  $realpath_cache_remaining = ($realpath_cache_size - $realpath_cache_used);

  if ($realpath_cache_remaining < 10) {
    // Set alert if less then 10 bytes are free.
    $alerts[] = array(
      'title' => t('Out of memory'),
      'message' => t('Real path cache needs more memory (free: @free KBytes). You should set the "realpath_cache_size" variable in your php.ini to a higher value.', array('@free' => $realpath_cache_remaining)),
    );
  }

  return $alerts;
}
