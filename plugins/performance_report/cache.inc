<?php
/**
 * @file
 * Gets information about Drupal's standard performance settings.
 */

$plugin = array(
  'requirements' => array(
    'function' => 'performance_report_cache_requirements',
  ),
  'data' => array(
    'function' => 'performance_report_cache_data',
  ),
  'alerts' => array(
    'function' => 'performance_report_cache_alerts',
  ),
);

/**
 * Checkes for requitement, which in this case will be that Drupal is installed,
 * which is think we don't need to check.
 *
 * @return array
 *  Empty array as no requirements exists.
 */
function performance_report_cache_requirements() {
  return array();
}

/**
 * Get information about Drupal's performance settings.
 *
 * @return array
 *  Render array with Drupal settings information.
 */
function performance_report_cache_data() {
  $data = array(
    'cache' => array(
      'title' => t('Page cache'),
      'value' => variable_get('cache', FALSE) ? t('Enabled') : '<strong>' . t('Disabled') . '</strong>',
    ),
    'page_compression' => array(
      'title' => t('Page Compression'),
      'value' => variable_get('page_compression', FALSE) ? t('Enabled') : '<strong>' . t('Disabled') . '</strong>',
    ),
    'block_cache' => array(
      'title' => t('Block cache'),
      'value' => variable_get('block_cache', FALSE) ? t('Enabled') : '<strong>' . t('Disabled') . '</strong>',
    ),
    'preprocess_css' => array(
      'title' => t('Preprocess CSS'),
      'value' => variable_get('preprocess_css', FALSE) ? t('Enabled') : '<strong>' . t('Disabled') . '</strong>',
    ),
    'preprocess_js' => array(
      'title' => t('Preprocess JavaScript'),
      'value' => variable_get('preprocess_js', FALSE) ? t('Enabled') : '<strong>' . t('Disabled') . '</strong>',
    ),
    'cache_lifetime' => array(
      'title' => t('Minimum cache lifetime'),
      'value' => variable_get('cache_lifetime', 'None'),
    ),
    'page_cache_maximum_age' => array(
      'title' => t('Maximum cache lifetime'),
      'value' => variable_get('page_cache_maximum_age', 'None'),
    ),
  );

  return array(
    '#theme' => 'performance_report',
    '#title' => t('Drupal settings'),
    '#description' => t("Check Drupal's performance settings."),
    '#data' => $data,
  );
}

/**
 * Goes through Drupal's performance settings to check that they are actived.
 *
 * @return array $alerts
 *  An array of alert messages if any are found.
 */
function performance_report_cache_alerts() {
  $alerts = array();

  // Page cache.
  if (!variable_get('page_compression', FALSE)) {
    $alerts[] = array(
      'title' => t('No page compression'),
      'message' => t('<strong>Drupal</strong>: page compression is <strong>not</strong> activated.'),
    );
  }
  else {
    // Block cache.
    if (!variable_get('block_cache', FALSE)) {
      $alerts[] = array(
        'title' => t('No block page cache'),
        'message' => t('<strong>Drupal</strong>: block cache is <strong>not</strong> activated.'),
      );
    }
  }

  // Block cache.
  if (!variable_get('block_cache', FALSE)) {
    $alerts[] = array(
      'title' => t('No block page cache'),
      'message' => t('<strong>Drupal</strong>: block cache is <strong>not</strong> activated.'),
    );
  }

  // Preprocess css.
  if (!variable_get('preprocess_css', FALSE)) {
    $alerts[] = array(
      'title' => t('No CSS aggregation'),
      'message' => t('<strong>Drupal</strong>: CSS aggregation and compression is <strong>not</strong> activated.'),
    );
  }

  // Preprocess css.
  if (!variable_get('preprocess_js', FALSE)) {
    $alerts[] = array(
      'title' => t('No JavaScript aggregation'),
      'message' => t('<strong>Drupal</strong>: JavaScript aggregation is <strong>not</strong> activated.'),
    );
  }

  return $alerts;
}
