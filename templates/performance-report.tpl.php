<?php
/**
 * @file
 *
 */
?>
<fieldset class="collapsible form-wrapper <?php print $collapsed ? 'collapsed' : '' ?>">
  <legend>
    <span class="fieldset-legend"><?php print $title; ?></span>
  </legend>
  <div class="fieldset-wrapper">
    <?php if ($data): ?>
    <ul>
      <?php foreach ($data as $key => $value): ?>
      <li class="<?php print str_replace('_', '-', $key); ?>">
      <?php echo $value['title'], ': ', $value['value']; ?>
      </li>
      <?php endforeach; ?>
    </ul>
    <?php endif; ?>
    <?php
    if ($children) {
      print drupal_render($children);
    }
    ?>
    <?php if ($actions): ?>
    <div class="actions">
      <p><?php print drupal_render($actions); ?></p>
    </div>
    <?php endif; ?>
  </div>
</fieldset>