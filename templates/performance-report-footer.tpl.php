<?php
/**
 * @file
 *
 */
?>
<div class="<?php print $classes; ?>">
  <h3><?php print $title; ?></h3>
  <?php if ($data): ?>
  <ul>
    <?php foreach ($data as $key => $value): ?>
    <li class="<?php print str_replace('_', '-', $key); ?>">
    <?php echo $value['title'], ': ', $value['value']; ?>
    </li>
    <?php endforeach; ?>
  </ul>
  <?php endif; ?>
</div>