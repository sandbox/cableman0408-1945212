This module tries to give an report about the current performance settings and
tries to come with some recommendation related to performance extension that
every Drupal site should be using to boots performance.

# Plug-ins
The modules uses ctools as plug-in system and comes as default with support for
checking the performance settings/stats for the list below.

 * APC
 * Drupal performance settings
 * Memcache
 * Real path cache
 * Varnish

# Installation
Download the module and install as any other Drupal module.

# Usage
You will find the performance report under "reports/performance_report" after
the module have been enabled.

# Creating new plug-ins
The plug-ins are standard ctools plug-ins that implements the functions listed
below. The requirements and data function is required for the plug-in to work
correctly.

For more information please see the APC extension and read the ctools plug-in
information available in ctools when advanced help module is install.

$plugin = array(
  'requirements' => array(
    // Required.
    'function' => 'MODULE_NAME_PLUGIN_requirements',
  ),
  'data' => array(
    // Required.
    'function' => 'MODULE_NAME_PLUGIN_data',
  ),
  'alerts' => array(
    // Optional.
    'function' => 'MODULE_NAME_PLUGIN_alerts',
  ),
  'menu' => array(
    // Optional.
    'function' => 'MODULE_NAME_PLUGIN_menu',
  ),
);
